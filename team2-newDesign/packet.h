#include <cstdio>
#include <vector>

using std::vector;

#define PACKET_SIZE 1480
#define PAYLOAD_SIZE 1470
#define NACK_SIZE 366
#define WINDOW_SIZE 68

typedef struct  tagDataPacket{
	long seq_num;
	bool eof;
	char data[PAYLOAD_SIZE];
}DataPacket;

typedef struct tagStartPacket{
	char filename[50];
	char my_hostname[100];
	int my_portno;
	long num_packets;
}StartPacket;

typedef struct tagControlPacket{
	long nack_num;
}ControlPacket;
