#include <cstdio>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h> 
#include "packet.h"


void error(const char *msg)
{
    perror(msg);
    exit(0);
}

int main(int argc, char *argv[])
{
    int sockfd, portno, n;
    struct sockaddr_in serv_addr;
    struct hostent *server;
    socklen_t len;
    FILE *fd;

    Packet send_pkt, *recv_pkt;
    
    char buffer[200];
    if (argc < 4) {
       fprintf(stderr,"usage %s hostname port filename\n", argv[0]);
       exit(0);
    }
    portno = atoi(argv[2]);
    sockfd = socket(AF_INET, SOCK_DGRAM, 0);
    if (sockfd < 0) 
        error("ERROR opening socket");
    server = gethostbyname(argv[1]);
    if (server == NULL) {
        fprintf(stderr,"ERROR, no such host\n");
        exit(0);
    }
    bzero((char *) &serv_addr, sizeof(serv_addr));
    serv_addr.sin_family = AF_INET;
    bcopy((char *)server->h_addr, 
         (char *)&serv_addr.sin_addr.s_addr,
         server->h_length);
    serv_addr.sin_port = htons(portno);

    //send_pkt.data = NULL;
    send_pkt.seq_num = 1;
    strcpy(send_pkt.filename, argv[3]);
    len = sizeof(struct sockaddr);
    n = sendto(sockfd, (void *)&send_pkt,sizeof(send_pkt),0,(struct sockaddr *) &serv_addr,len);
    if (n < 0) 
         error("ERROR writing to socket");
    printf("send to successful");

    //buffer = (char *)malloc(PACKET_SIZE);
    n = recvfrom(sockfd, buffer, PACKET_SIZE, 0,(struct sockaddr *) &serv_addr,&len);
    if (n < 0) 
         error("ERROR reading from socket");
//    memset(recv_pkt, 0, sizeof(*recv_pkt));

    recv_pkt = (Packet *)buffer;

    printf("Filename : %s", recv_pkt->filename);
    //printf("Data : %s", (char *)(recv_pkt->data));

    char * pch, prev[50];
    bzero(prev, 50);
    pch = strtok (argv[3],"/");
    while (pch != NULL) {
	    strcpy(prev, pch);
	    pch = strtok (NULL, "/");
    }
    fd = fopen(prev, "w+");
    if(fd < 0)
	    error("Error in opening file");
    n = fwrite(recv_pkt->data, 100, 1, fd);
    if(n < 0)
	    error("Error writing to file");	    

    fclose(fd);
    close(sockfd);
    return 0;
}
