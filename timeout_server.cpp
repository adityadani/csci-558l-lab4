/* A simple server in the internet domain using TCP
   The port number is passed as an argument */
#include <cstdio>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h> 
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include "packet.h"
#include <time.h>
#include <stdbool.h>

void error(const char *msg)
{
    perror(msg);
    exit(1);
}


int main(int argc, char *argv[])
{
  int sockfd, newsockfd, portno,result=0;
     FILE *fd;
     socklen_t clilen;
     bool b_val=true;
     char host[50], service[50], filename[50];
     char buffer[200];
     struct sockaddr_in serv_addr, cli_addr;
     struct timeval tv;
     int n;
     int dtime=1000;
     socklen_t len;

     Packet *pkt, send_pkt; 


     if (argc < 2) {
         fprintf(stderr,"ERROR, no port provided\n");
         exit(1);
     }
     sockfd = socket(AF_INET, SOCK_DGRAM, 0);
     if (sockfd < 0) 
        error("ERROR opening socket");
     bzero((char *) &serv_addr, sizeof(serv_addr));
     portno = atoi(argv[1]);
     serv_addr.sin_family = AF_INET;
     serv_addr.sin_addr.s_addr = INADDR_ANY;
     serv_addr.sin_port = htons(portno);
     bind(sockfd, (struct sockaddr *) &serv_addr,sizeof(serv_addr));
     tv.tv_sec=100;
     if ( setsockopt(sockfd,SOL_SOCKET,SO_RCVTIMEO,(char*)&(tv.tv_sec),sizeof(tv)))
     error("ERROR on setsockopt");
     printf("I am Ready!!!!!!!");
     len = sizeof(struct sockaddr);
     while(1) {
	     printf("I am Ready");
	     //buffer = (char *)malloc(PACKET_SIZE);
	     n = recvfrom(sockfd, buffer, PACKET_SIZE, 0, (struct sockaddr *)&cli_addr, &len);
	     
	     printf("sockfd %d", sockfd);
	     if (n < 0) error("ERROR reading from socket");

	     pkt = (Packet *)buffer;
	     strcpy(filename, pkt->filename);

	     fd = fopen(filename, "r");
	     if(fd < 0)
		     error("Error opening file");

	     //send_pkt.data = malloc(100);
	     int bytes_read = fread(send_pkt.data, 100, 1, fd);
	     if(bytes_read < 0)
		     error("Error reading file");

	     send_pkt.seq_num = 1;
	     strcpy(send_pkt.filename, filename);
	     send_pkt.eof = true;

	     //getnameinfo((struct sockaddr *)&cli_addr, sizeof(struct sockaddr), host, 50, service, 50, 0);
	     n = sendto(sockfd,(void *)&send_pkt, sizeof(send_pkt), 0, (struct sockaddr *)&cli_addr, len);
	     if (n < 0) error("ERROR writing to socket");
	     }
     close(sockfd);
     return 0; 
}

