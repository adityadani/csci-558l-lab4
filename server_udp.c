/* A simple server in the internet domain using TCP
   The port number is passed as an argument */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h> 
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>

void error(const char *msg)
{
    perror(msg);
    exit(1);
}



int main(int argc, char *argv[])
{
     int sockfd, newsockfd, portno;
     socklen_t clilen;
     char buffer[256], host[50], service[50];
     struct sockaddr_in serv_addr, cli_addr;
     int n;
     socklen_t len;

     if (argc < 2) {
         fprintf(stderr,"ERROR, no port provided\n");
         exit(1);
     }
     sockfd = socket(AF_INET, SOCK_DGRAM, 0);
     if (sockfd < 0) 
        error("ERROR opening socket");
     bzero((char *) &serv_addr, sizeof(serv_addr));
     portno = atoi(argv[1]);
     serv_addr.sin_family = AF_INET;
     serv_addr.sin_addr.s_addr = INADDR_ANY;
     serv_addr.sin_port = htons(portno);
     if (bind(sockfd, (struct sockaddr *) &serv_addr,
              sizeof(serv_addr)) < 0) 
              error("ERROR on binding");
     len = sizeof(struct sockaddr);
     while(1) {
	     printf("I am Ready");
	     bzero(buffer,256);
	     n = recvfrom(sockfd,buffer,255, 0, (struct sockaddr *)&cli_addr, &len);
	     if (n < 0) error("ERROR reading from socket");
	     getnameinfo((struct sockaddr *)&cli_addr, sizeof(struct sockaddr), host, 50, service, 50, 0);
	     n = sendto(sockfd,"I got your message",18, 0, (struct sockaddr *)&cli_addr, len);
	     if (n < 0) error("ERROR writing to socket");
     }
     close(sockfd);
     return 0; 
}
