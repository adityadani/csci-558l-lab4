#include <cstdio>

#define PACKET_SIZE 200
#define PAYLOAD_SIZE 100

class Packet {

 public:
	int seq_num;
	char filename[50];

	Packet(){}
	~Packet(){}
};

class CPacket : public Packet {
 public:
	bool ack;
	CPacket(){}
	~CPacket(){}

};

class DPacket : public Packet {
 public:
	bool eof;
	char data[PAYLOAD_SIZE];
	DPacket(){}
	~DPacket(){}

};
