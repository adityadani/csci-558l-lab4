/* A simple server in the internet domain using TCP
   The port number is passed as an argument */
#include <cstdio>
#include <iostream>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h> 
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include "packet.h"
#include <math.h>

#include <sys/time.h>

using namespace std;

double read_timer( )
{
    static bool initialized = false;
    static struct timeval start;
    struct timeval end;
    if( !initialized )
    {
        gettimeofday( &start, NULL );
        initialized = true;
    }

    gettimeofday( &end, NULL );

    return (end.tv_sec - start.tv_sec) + 1.0e-6 * (end.tv_usec - start.tv_usec);
}

void error(const char *msg)
{
    perror(msg);
    exit(1);
}

int main(int argc, char *argv[])
{	
	int window_packet_count=1;
	int end_flag=0;
	int sockfd, newsockfd, portno;
    FILE *fd;
    double start_time,elapsed_time;
    socklen_t clilen;
    char host[50], service[50], filename[50];
    char buffer[PACKET_SIZE], data_from_file[PAYLOAD_SIZE];
    struct sockaddr_in serv_addr, cli_addr;
    int n, seq=1, bytes_read=0;
    socklen_t len;

    ControlPacket *recv_pkt;
    DataPacket  send_pkt; 
    StartPacket req_pkt;

    if (argc < 2) {
        fprintf(stderr,"ERROR, no port provided\n");
    	exit(1);
    }
    printf("I am Ready!!!!");
    sockfd = socket(AF_INET, SOCK_DGRAM, 0);
    if (sockfd < 0) 
       error("ERROR opening socket");
    bzero((char *) &serv_addr, sizeof(serv_addr));
    portno = atoi(argv[1]);
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_addr.s_addr = INADDR_ANY;
    serv_addr.sin_port = htons(portno);
    if (bind(sockfd, (struct sockaddr *) &serv_addr,
        sizeof(serv_addr)) < 0) 
        error("ERROR on binding");
    len = sizeof(struct sockaddr);
    double count=0;
	vector<int> myqueue;
	vector<int>::iterator it;
	printf("I am Ready");
	n = recvfrom(sockfd, buffer, PACKET_SIZE, 0, (struct sockaddr *)&cli_addr, &len);
	printf("sockfd %d", sockfd);
	if (n < 0) error("ERROR reading from socket");
	  
	req_pkt = (StartPacket *)buffer;
	printf("\n Filename: %s \n", req_pkt->filename);
	strcpy(filename, req_pkt->filename);
	     
	start_time= read_timer();
	fd = fopen(filename, "r");
	if(fd < 0)
	    error("Error opening file");
	fseek(fd,0,SEEK_END);
	double last_sent=1;
	double no_of_bytes = ftell(fd);
	double no_of_packets = ceil(no_of_bytes/PAYLOAD_SIZE);
	printf("\n The number of packets %f \n",no_of_packets);
	fseek(fd,0,SEEK_SET);
	do{
			while(window_packet_count <= WINDOW_SIZE)
			{
				if(myqueue.size()!=0){
					for(it = myqueue.begin();it != myqueue.end(); it++){
					fseek(fd,((*it)-1)*PAYLOAD_SIZE , SEEK_SET);			
					bytes_read = fread(data_from_file,1, PAYLOAD_SIZE, fd);
                	cout << bytes_read<<endl;
	                if(bytes_read < 0) {
                      		error("Error reading file");
       				}
	       	                     	memset(send_pkt.data, '\0', PAYLOAD_SIZE);
                	                strncpy(send_pkt.data, data_from_file, bytes_read);
                        	     	memset(data_from_file,'\0', PAYLOAD_SIZE);
                    //USE SERIALIZE
                     			send_pkt.seq_num = seq++;
	                     		strcpy(send_pkt.filename, filename);
                     //cout << "Count"<<endl;
                	     		send_pkt.fileSize = no_of_bytes;
					if(window_packet_count ==WINDOW_SIZE){
						send_pkt.eow = true;
					}
					n = sendto(sockfd,(void *)&send_pkt, sizeof(send_pkt), 0, (struct sockaddr *)&cli_addr, len);
					window_packet_count++;
			}
			}
			fseek(fd,(last_sent-1)*PAYLOAD_SIZE,SEEK_SET);
		    bytes_read = fread(data_from_file,1, PAYLOAD_SIZE, fd);
			cout << bytes_read <<endl;
		    if(bytes_read < 0) {
		     	error("Error reading file");
		    }
		    
			memset(send_pkt.data, '\0', PAYLOAD_SIZE);							
			strncpy(send_pkt.data, data_from_file, bytes_read);
			memset(data_from_file,'\0', PAYLOAD_SIZE);
		    //USE SERIALIZE
		    send_pkt.seq_num = seq++;
		    strcpy(send_pkt.filename, filename);
		    //cout << "Count"<<endl;
		    send_pkt.fileSize = no_of_bytes; 
		    count++;
		    if(count == no_of_packets){
			   	send_pkt.eof = true;
				end_flag=1;
		    }
		    else  send_pkt.eof = false;
		    //printf("\n Sending data : %s \n",send_pkt.data); 
		    cout<<" Seqno: "<<count<<endl;
			cout<<"sizeof : "<<sizeof(send_pkt)<<endl;
		    n = sendto(sockfd,(void *)&send_pkt, sizeof(send_pkt), 0, (struct sockaddr *)&cli_addr, len);
		    if (n < 0) 
			    error("ERROR writing to socket");
			window_packet_count++;
			cout<<endl<<"Window packet size;"<<window_packet_count<<endl;
			if(end_flag==1) break;
			last_sent = seq;
			}

			//inner while

			window_packet_count=1;
			cout<<" tttqwwqeiiiiiiiiiiii1"<<endl;
			
			//--------Waiting for negative ack after every window---------
		    n = recvfrom(sockfd, buffer, PACKET_SIZE, 0, (struct sockaddr *)&cli_addr, &len);		     
		    cout<<"I am out of the windows while loop"<<endl; 
		    if (n < 0) 
			    error("ERROR reading from socket");


		//---------- Resending lost packets -----------------


		    recv_pkt = (ControlPacket *)buffer;
			
			myqueue = recv_pkt->nack_vector;
			
			if(recv_pkt->ack == false) {
                             fseek(fd, -PAYLOAD_SIZE, SEEK_CUR);
                    }
			
		     //if( recv_pkt->eof == true) 
			if(end_flag==1)	{
				elapsed_time = read_timer() - start_time;
				printf("\n --------- File Transfer Complete -------- \n The elapsed time is %f\n", elapsed_time);
				break;
		    }
	    }while(recv_pkt->eof!=1);
     
    close(sockfd);
    return 0; 
}

