#include <cstdio>
#include <iostream>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h> 
#include <algorithm>
#include <math.h>
#include "packet.h"
#include <vector>

using namespace std;

vector<int> myqueue;
vector<int>::iterator it;
int total_count = 1;

void error(const char *msg)
{
    perror(msg);
    exit(0);
}


int main(int argc, char *argv[])
{
	int window_packet_count=1;
    int sockfd, portno, n;
    struct sockaddr_in serv_addr;
    struct hostent *server;
    socklen_t len;
    FILE *fd;
    int received_bytes, exp_seq_num = 1;
	int xyz=1;
    
   // RequestPacket req_pkt;
    DataPacket *recv_pkt;
	StartPacket req_pkt;
        ControlPacket *pkt_with_nacks, *last_pkt;
          
    char prev[50], *pch;
    char buffer[PACKET_SIZE];
    if (argc < 4) {
       fprintf(stderr,"usage %s hostname port filename\n", argv[0]);
       exit(0);
    }
    portno = atoi(argv[2]);
    sockfd = socket(AF_INET, SOCK_DGRAM, 0);
    if (sockfd < 0) 
        error("ERROR opening socket");
    server = gethostbyname(argv[1]);
    if (server == NULL) {
        fprintf(stderr,"ERROR, no such host\n");
        exit(0);
    }
    bzero((char *) &serv_addr, sizeof(serv_addr));
    serv_addr.sin_family = AF_INET;
    bcopy((char *)server->h_addr, 
    (char *)&serv_addr.sin_addr.s_addr,
    server->h_length);
    serv_addr.sin_port = htons(portno);
    double num_of_packets;
    int next=1;


    //req_pkt.seq_num = 1;
    strcpy(req_pkt.filename, argv[3]);
    len = sizeof(struct sockaddr);


	//Send filename to the client
    n = sendto(sockfd, (void *)&req_pkt,sizeof(req_pkt),0,(struct sockaddr *) &serv_addr,len);

    //Create file and write into that file
    if (n < 0) 
        error("ERROR writing to socket");
    printf("\n Sent file name \n");

    pch = strtok (argv[3],"/");
    while (pch != NULL) {
	    strcpy(prev, pch);
	    pch = strtok (NULL, "/");
    }
    fd = fopen(prev, "w");


	//-------------Outer Loop-----------------------
	// Receive first datagram from the server

	memset(buffer, '\0', PACKET_SIZE);
	received_bytes = recvfrom(sockfd, buffer, PACKET_SIZE, 0,(struct sockaddr *) &serv_addr,&len);

	if (received_bytes < 0) 
		    error("ERROR reading from socket");

	recv_pkt = (DataPacket *)buffer;
	
	double count = recv_pkt->no_of_packets;
	int bitmap[(long)count];
	for(int i=0;i< (long)count ;i++){
		bitmap[i]=0;
	}
    do {
	
	while(1)
	{
	    if(recv_pkt->seq_num > next){     	    
			    for(int i=next;i<= (recv_pkt->seq_num -1 );i++)
				    myqueue.push_back(i);
			    next = recv_pkt->seq_num + 1;
		}		else if(recv_pkt->seq_num < next){
			   	if(myqueue.size()!=0){
			    it = std::find(myqueue.begin(),myqueue.end(),recv_pkt->seq_num);
			    if(it!=myqueue.end())
				myqueue.erase(it);
		   		} 
		}		
		else {
			next = recv_pkt->seq_num + 1; 
		}
			
		    // fd = fopen(argv[3], "w");
			cout<<"hereee!!!"<<endl;
		    fseek(fd,(recv_pkt->seq_num-1)*PAYLOAD_SIZE,SEEK_SET);
		    n = fwrite(recv_pkt->data, PAYLOAD_SIZE, 1, fd);
		    cout<<endl<<"Here 1"<<endl;
			if(n < 0)
			    error("Error writing to file");
/*
	    if(recv_pkt->seq_num != exp_seq_num) {
		    continue;
	    }
	    else {
		    exp_seq_num++;
	    }

		printf("\nReceived data :\n%s\n", recv_pkt->data);
	    n = fwrite(recv_pkt->data, PAYLOAD_SIZE, 1, fd);
	    if(n < 0)
		    error("Error writing to file");

*/		
		if(bitmap[(long)(recv_pkt->seq_num)]==0){
			window_packet_count++;
			total_count++;
			bitmap[(long)(recv_pkt->seq_num)] = 1;
		}

		cout<<endl<<"Window Packet Count "<<window_packet_count<<endl;
		if(recv_pkt->eof == true){
			cout<<endl<<"End of file"<<endl;
			break;
		
		}
		if(recv_pkt->eow == true) break;
		received_bytes = recvfrom(sockfd, buffer, PACKET_SIZE, 0,(struct sockaddr *) &serv_addr,&len);
		if (received_bytes < 0) 
		    error("ERROR reading from socket");

		recv_pkt = (DataPacket *)buffer;

	}
	//End of inner while

	
		cout<<endl<<"Here 2"<<endl;
		window_packet_count=1;
	    printf("\nSending NACKS\n"); 
	    send_pkt.ack = true;
	    ControlPacket *pkt_with_nacks;
	    
     pkt_with_nacks = (ControlPacket *)malloc(sizeof(struct tagControlPacket));
		cout<<endl<<"Here 3"<<endl;
		// if(myqueue.size() == 0)
                  //  break;
        pkt_with_nacks->nack_vector = myqueue;
        pkt_with_nacks->seq_num = xyz++;

		printf("I am here!!!!!!!!!!!!1111111111111111111111");
		if(total_count < num_of_packets)
		{            
		n = sendto(sockfd, (void *)pkt_with_nacks,sizeof(ControlPacket),0,(struct sockaddr *) &serv_addr,len);
	    if (n < 0) 
		    error("ERROR writing to socket");
	    
	    n = sendto(sockfd, (void *)pkt_with_nacks,sizeof(ControlPacket),0,(struct sockaddr *) &serv_addr,len);
	    if (n < 0) 
		    error("ERROR writing to socket");
	    
	    n = sendto(sockfd, (void *)pkt_with_nacks,sizeof(ControlPacket),0,(struct sockaddr *) &serv_addr,len);
	    if (n < 0) 
		    error("ERROR writing to socket");
	    
	    n = sendto(sockfd, (void *)pkt_with_nacks,sizeof(ControlPacket),0,(struct sockaddr *) &serv_addr,len);
	    if (n < 0) 
		    error("ERROR writing to socket");
	    
	    n = sendto(sockfd, (void *)pkt_with_nacks,sizeof(ControlPacket),0,(struct sockaddr *) &serv_addr,len);
	    if (n < 0) 
	
	    error("ERROR writing to socket");
	    
	}
	else
	{
		pkt_with_nacks->eof = true;
		n = sendto(sockfd, (void *)pkt_with_nacks,sizeof(ControlPacket),0,(struct sockaddr *) &serv_addr,len);
            if (n < 0)
                    error("ERROR writing to socket");
		n = sendto(sockfd, (void *)pkt_with_nacks,sizeof(ControlPacket),0,(struct sockaddr *) &serv_addr,len);
            if (n < 0)
                    error("ERROR writing to socket");
		n = sendto(sockfd, (void *)pkt_with_nacks,sizeof(ControlPacket),0,(struct sockaddr *) &serv_addr,len);
            if (n < 0)
                    error("ERROR writing to socket");
		n = sendto(sockfd, (void *)pkt_with_nacks,sizeof(ControlPacket),0,(struct sockaddr *) &serv_addr,len);
            if (n < 0)
                    error("ERROR writing to socket");

		n = sendto(sockfd, (void *)pkt_with_nacks,sizeof(ControlPacket),0,(struct sockaddr *) &serv_addr,len);
            if (n < 0)
                    error("ERROR writing to socket");
		break;
	}
	
	
    }while(total_count < num_of_packets);

    fclose(fd);

    close(sockfd);
    return 0;
}

/*
int count = num_of_packets;
    int next = 1;
    len = sizeof(struct sockaddr);
    while(count>0) {
	    while(i < WINDOW_SIZE) {
		    n = recvfrom(udpsockfd,buffer, PACKET_SIZE, 0,(struct sockaddr *) &serv_addr, &len);
		    if (n < 0) 
			    error("ERROR reading from socket");
		    
		    recv_pkt = (Packet *)buffer;
		    
		    if(recv_pkt->seq_num > next){     	    
			    for(int i=next;i<= (recv_pkt->seq_num -1 );i++)
				    myqueue.push_back(i);
			    next = recv_pkt->seq_num + 1;
		    }else if(recv_pkt->seq_num < next){
			    
			    it = std::find(myqueue.begin(),myqueue.end(),recv_pkt->seq_num);
			    myqueue.erase(it);
		    }else {  
			    next = recv_pkt->seq_num + 1;  
		    }
		    
		    
		    // fd = fopen(argv[3], "w");
		    fseek(fd,(num_of_packets-count+1)*PAYLOAD_SIZE,SEEK_SET);
		    n = fwrite(recv_pkt->data, PAYLOAD_SIZE, 1, fd);
		    if(n < 0)
			    error("Error writing to file");
		    count--;
		    if(count == 0)
			    break;
		    i++;
	    }
	    i = 0;

	    // Access the condition variable
	    if(myqueue.size() != 0) {
		    pthread_mutex_lock(&cond_mutex);
		    pthread_cond_signal(&wake_tcp);
		    pthread_mutex_unlock(&cond_mutex);
	    }
    }
*/

