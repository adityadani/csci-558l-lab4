#include <cstdio>
#include <vector>

using std::vector;

#define PACKET_SIZE 1480
#define PAYLOAD_SIZE 1470
#define NACK_SIZE 366
#define WINDOW_SIZE 68

typedef struct  tagDataPacket{
	long seq_num;
	bool eof;
	char data[PAYLOAD_SIZE];
}DataPacket;

typedef struct tagStartPacket{r
	char filename[50];
	long no_of_packets;
}ServerStartPacket;

typedef struct tagStartPacket{r
	char filename[50];
	long no_of_packets;
}ClientStartPacket;



typedef struct tagControlPacket{
	long seq_num;
	int num_nacks;
	bool eof;
	int nack_array[NACK_SIZE];
	vector<long> nack_vector;
}ControlPacket;
