/* A simple server in the internet domain using TCP
   The port number is passed as an argument */
#include <cstdio>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h> 
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include "packet.h"
#include <pthread.h>
#include <unistd.h>
#include <sys/time.h>
#include <math.h>
#include <algorithm>

pthread_mutex_t flag_mutex;
bool ack_recv_flag;
int num;
int port_udp, port_tcp;
int num_pkts;
char filename[50];
vector <int> lost_packets;
char client_hostname[100];
int client_portno;

vector<int>::iterator it;

void error(const char *msg)
{
    perror(msg);
    exit(1);
}


void *udpThread(void *id) {
	int i=0, n, bytes_read;
	int udpsockfd, portno;
	socklen_t clilen;
	char buffer[PACKET_SIZE];
	struct sockaddr_in serv_addr, cli_addr, my_addr;
	socklen_t len;
	struct hostent *client;
	DataPacket *send_pkt;
	FILE *fd;
	send_pkt = (DataPacket *)malloc(sizeof(struct tagDataPacket)); 
	
	//--------------opening udp connection---------------------
	udpsockfd = socket(AF_INET, SOCK_DGRAM, 0);
	if (udpsockfd < 0) 
		error("ERROR opening socket");
	
	
	int seq;
	
	client = gethostbyname(client_hostname);
	if (client == NULL) {
		fprintf(stderr,"ERROR, no such host\n");
		exit(0);
	}
	bzero((char *) &my_addr, sizeof(my_addr));
	my_addr.sin_family = AF_INET;
	bcopy((char *)client->h_addr, 
	      (char *)&my_addr.sin_addr.s_addr,
	      client->h_length);
	my_addr.sin_port = htons(client_portno);
	
	
	int count = 1;
	int remaining_numOf_pkts = num_pkts;
	
	int buffersize;
	socklen_t bufferlen;
	bufferlen = sizeof(buffersize);
	int res = getsockopt(udpsockfd, SOL_SOCKET, SO_SNDBUF, &buffersize, &bufferlen);
	printf("\nMy buffer size : %d", buffersize);
	
	int sendbuff = 131071;
	printf("\nSetting to size : %d", sendbuff);
	//setsockopt(udpsockfd, SOL_SOCKET, SO_SNDBUF, &sendbuff, sizeof(sendbuff));
	res = getsockopt(udpsockfd, SOL_SOCKET, SO_SNDBUF, &buffersize, &bufferlen);
	printf("\nMy buffer size : %d", buffersize);
	
	
	
	len = sizeof(struct sockaddr);
	fd = fopen(filename, "r");
	while(1) {
		if(remaining_numOf_pkts > 0) {
			while(i < WINDOW_SIZE) {
				bzero(send_pkt->data, PAYLOAD_SIZE);
				n=fread(send_pkt->data, PAYLOAD_SIZE, 1, fd);
				send_pkt->seq_num = count;
				//strcpy(send_pkt.filename, filename);
				
				printf("\nSeq num : %ld i : %d", send_pkt->seq_num, i);
				send_pkt->eof = false;
				if( count == num_pkts)       // 'size' has the number of packets
					send_pkt->eof = true;
				count++;
				remaining_numOf_pkts--;
				
				bzero(buffer, PACKET_SIZE);
				memcpy(buffer, send_pkt, sizeof(struct tagDataPacket));
				n = sendto(udpsockfd, buffer, PACKET_SIZE, 0, (struct sockaddr *)&my_addr, len);
				if (n < 0) 
					error("ERROR writing to socket");
				if(remaining_numOf_pkts <= 0)
					break;
				
				i++;
			}
			i = 0;
			usleep(1000);
			
			pthread_mutex_lock(&flag_mutex);
			printf("\nGot a lock");
			if(ack_recv_flag == true) {
				printf("\nAck flag true");
				if(lost_packets.empty()) {
					pthread_mutex_unlock(&flag_mutex);
					break;
				}
				//send the missing packets.
				while(!lost_packets.empty()) {
					printf("\nSize of lost packets : %ld", lost_packets.size());
					seq = lost_packets.front();
					it = std::find(lost_packets.begin(),lost_packets.end(),seq);
					lost_packets.erase(it);
					fseek(fd,(seq*PAYLOAD_SIZE),SEEK_SET);
					fread(send_pkt->data, PAYLOAD_SIZE, 1, fd);
					send_pkt->seq_num = seq;
					send_pkt->eof = false;
					if(num_pkts == seq)
						send_pkt->eof = true;
					n = sendto(udpsockfd,(void *)send_pkt, sizeof(struct tagDataPacket), 0, (struct sockaddr *)&my_addr, len);
					if (n < 0) error("ERROR writing to socket");	
				}
				fseek(fd, (count*PAYLOAD_SIZE), SEEK_SET);
				ack_recv_flag = false;
			}	
			pthread_mutex_unlock(&flag_mutex);
			if(remaining_numOf_pkts <= 0)
				usleep(1000000); // Check for this value;			
		}	     
	}
	close(udpsockfd);
	fclose(fd);
	pthread_exit(NULL);
}

void *tcpThread(void *id) {
     
     int tcpsockfd,portno_tcp;
     int childsockfd;
     socklen_t clilen;
     char buffer[PACKET_SIZE];
     struct sockaddr_in serv_addr, cli_addr_tcp;
     int n;

     socklen_t len;
     int bytes_read;
     StartPacket *start_recv_pkt, *start_send_pkt;
     ControlPacket *c_pkt;
     c_pkt = (ControlPacket *)malloc(sizeof(struct tagControlPacket));
     start_recv_pkt = (StartPacket *)malloc(sizeof(struct tagStartPacket));
     start_send_pkt = (StartPacket *)malloc(sizeof(struct tagStartPacket));
     long t1=1;
     pthread_t udp_thread;
     FILE *fd;

     //-------opening tcp connection---------------------
	    
     printf("tcp : socket");
     
     tcpsockfd = socket(AF_INET, SOCK_STREAM, 0);
     
     if (tcpsockfd < 0) 
	     error("ERROR opening socket");
     
     bzero((char *) &serv_addr, sizeof(serv_addr));
     
     serv_addr.sin_family = AF_INET;
     serv_addr.sin_addr.s_addr = INADDR_ANY;
     serv_addr.sin_port = htons(port_tcp);
     
     if (bind(tcpsockfd, (struct sockaddr *) &serv_addr,					      
	      sizeof(serv_addr)) < 0) 
	     error("ERROR on binding");
     printf("tcp : bind");

     listen(tcpsockfd,5);
     
     clilen = sizeof(cli_addr_tcp);
     
     printf("tcp : waiting for accept");
     childsockfd = accept(tcpsockfd, (struct sockaddr *) &cli_addr_tcp, 
			&clilen);
     if (childsockfd < 0) 
	     error("ERROR on accept");
     
     printf("tcp : return from accept");
     len = sizeof(struct sockaddr);
     
     bzero(buffer,PACKET_SIZE);
     printf("tcp : waiting for recv");
     n = recv(childsockfd, buffer, PACKET_SIZE, 0); 
     if (n < 0) error("ERROR reading from socket");

     memcpy(start_recv_pkt, buffer, sizeof(struct tagStartPacket));

     strcpy(filename, start_recv_pkt->filename);
     strcpy(client_hostname, start_recv_pkt->my_hostname);
     client_portno = start_recv_pkt->my_portno;
  
     fd = fopen(filename, "r");
     if(fd < 0)
	     error("Error opening file");
     fseek(fd, 0, SEEK_END);
     double size = ftell(fd);
     fseek(fd, 0, SEEK_SET); 
     
     int total_pkt = ceil(size/PAYLOAD_SIZE);

     printf("Count : %d", total_pkt);
     start_send_pkt->num_packets = total_pkt;
     num_pkts = total_pkt;
     fclose(fd);

     bzero(buffer, PACKET_SIZE);
     memcpy(buffer, start_send_pkt, sizeof(struct tagStartPacket));
     n = send(childsockfd, buffer, PACKET_SIZE, 0);
     if (n < 0) error("ERROR writing to socket"); 
     
     // Create UDP thread here!!!!

     pthread_create(&udp_thread, NULL, udpThread, (void *)t1);

     while(1) {
	     
	     bzero(c_pkt, sizeof(ControlPacket));
	     bzero(buffer, PACKET_SIZE);
	     n = recv(childsockfd, buffer, PACKET_SIZE, 0);
	     if (n < 0) error("ERROR writing to socket"); 
	     memcpy(c_pkt, buffer, sizeof(struct tagControlPacket));
	     //c_pkt = (CtrlPacket *)buffer;
	     
	     pthread_mutex_lock(&flag_mutex);
	     ack_recv_flag = true;

	     printf("server tcp wake up");
	     // Copy the vector into lost_packets
	     if(c_pkt->num_nacks == 0) {
		     printf("\nbreaking from here");
		     pthread_mutex_unlock(&flag_mutex);
		     break;
	     }
	     else {
		     int j = 0;
		     while(j < c_pkt->num_nacks) {
			     printf("size of nacks : %d", c_pkt->num_nacks);
			     lost_packets.push_back(c_pkt->nack_array[j]);
			     j++;
		     }
	     }
	     pthread_mutex_unlock(&flag_mutex);
     }
     pthread_join(udp_thread, NULL);
     close(tcpsockfd);
}

int main(int argc, char **argv) {
    long t1=1, t2=2;
    pthread_t udp_thread, tcp_thread;

     if (argc < 3) {
         fprintf(stderr,"ERROR, no port provided\n");
         exit(1);
     }
     port_udp = atoi(argv[2]);
     port_tcp = atoi(argv[1]);
     
     printf("\nPthread starting");
     pthread_mutex_init(&flag_mutex, NULL);
     
     pthread_create(&tcp_thread, NULL, tcpThread, (void *)t2);
     
     pthread_join(tcp_thread, NULL);
}
