#include <cstdio>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h> 
#include <pthread.h>
#include <algorithm>
#include "packet.h"

pthread_mutex_t cond_mutex;
pthread_cond_t wake_tcp;
pthread_t udp_thread, tcp_thread;

vector<int> myqueue;
vector<int>::iterator it;

int udpsockfd, tcpsockfd;
int server_port_tcp, server_port_udp;
char server_hostname[100], filename[100];
int num_of_packets;


void error(const char *msg)
{
    perror(msg);
    exit(0);
}


void *udpThread(void *id) {
	int i=0, n;

    // opening the file to write to
    char *pch, prev[50];
    struct sockaddr_in serv_addr;
    socklen_t len;
    FILE *fd;
    StartPacket *start_recv_pkt;
    DataPacket *data_pkt;
    char buffer[PACKET_SIZE];

    start_recv_pkt = (StartPacket *)malloc(sizeof(struct tagStartPacket));
    data_pkt = (DataPacket *)malloc(sizeof(struct tagDataPacket));
	    
    bzero(prev, 50);
    pch = strtok (filename,"/");
    while (pch != NULL) {
	    strcpy(prev, pch);
	    pch = strtok (NULL, "/");
    }
    fd = fopen(prev,"w");
    if(fd < 0)
	    error("Error in opening file");

    // Receive Data

     int buffersize;
     socklen_t bufferlen;
     bufferlen = sizeof(buffersize);
     int res = getsockopt(udpsockfd, SOL_SOCKET, SO_RCVBUF, &buffersize, &bufferlen);
     printf("\nMy buffer size : %d", buffersize);

     int sendbuff = 444444;
     printf("\nSetting to size : %d", sendbuff);
     setsockopt(udpsockfd, SOL_SOCKET, SO_RCVBUF, &sendbuff, sizeof(sendbuff));
     res = getsockopt(udpsockfd, SOL_SOCKET, SO_RCVBUF, &buffersize, &bufferlen);
     printf("\nMy buffer size : %d", buffersize);

    
    int count = num_of_packets;
    int next = 1;
    int flag = 0;
    len = sizeof(struct sockaddr);
    while(count>0) {
	    while(i < WINDOW_SIZE) {
		    bzero(data_pkt, sizeof(DataPacket));
		    bzero(buffer, PACKET_SIZE);
		    n = recvfrom(udpsockfd,buffer, PACKET_SIZE, 0,(struct sockaddr *) &serv_addr, &len);
		    if (n < 0) 
			    error("ERROR reading from socket");
		    
		    memcpy(data_pkt, buffer, sizeof(DataPacket));
		    
		    printf("\nSeq num : %ld", data_pkt->seq_num);		    
		    if(myqueue.size() != 0)
			    printf("\nPushing in vector next %d curr : %ld", next, data_pkt->seq_num);

		    if(data_pkt->seq_num > next){ 
			    printf("\nPushing in vector next %d curr : %ld", next, data_pkt->seq_num);
			    for(int i=next;i<= (data_pkt->seq_num -1 );i++)
				    myqueue.push_back(i);
			    next = data_pkt->seq_num + 1;
		    }else if(data_pkt->seq_num < next){
			    printf("\nSeq num erasing : %ld queue size : %ld", data_pkt->seq_num, myqueue.size());
			    it = std::find(myqueue.begin(),myqueue.end(),data_pkt->seq_num);
			    if(data_pkt->seq_num != 0)
				    myqueue.erase(it);
			    //printf("\nit erasing : %d", *it);
		    }else {  
			    next = data_pkt->seq_num + 1;  
		    }
		    
		    
		    // fd = fopen(argv[3], "w");
		    //fseek(fd,(num_of_packets-count+1)*PAYLOAD_SIZE,SEEK_SET);
		    fseek(fd,(data_pkt->seq_num - 1)*PAYLOAD_SIZE,SEEK_SET);
		    n = fwrite(data_pkt->data, strlen(data_pkt->data), 1, fd);
		    if(n < 0)
			    error("Error writing to file");
		    count--;
		    if(count == 0)
			    break;
		    i++;
	    }
	    i = 0;

	    // Access the condition variable
	    if(myqueue.size() != 0) {
		    printf("\n The vector is filled!!! %ld curr seq_num : %ld next : %d", myqueue.size(), data_pkt->seq_num, next);
		    pthread_mutex_lock(&cond_mutex);
		    pthread_cond_signal(&wake_tcp);
		    pthread_mutex_unlock(&cond_mutex);
	    }
	    else if(myqueue.size() == 0 && count == 0) {
		    pthread_mutex_lock(&cond_mutex);
		    pthread_cond_signal(&wake_tcp);
		    pthread_mutex_unlock(&cond_mutex);
		    break;
	    }
    }
    close(udpsockfd);
    fclose(fd);
    pthread_exit(0);
}

void *tcpThread(void *id) {	
	int i=0, n, count = 1;
	long t1;
	struct sockaddr_in serv_addr_tcp, serv_addr_udp, my_addr_udp;
	struct hostent *server;
	FILE *fd;
	StartPacket *start_send_pkt, *start_recv_pkt;
	ControlPacket *pkt_with_nacks, *last_pkt;
	char buffer[PACKET_SIZE];

	start_send_pkt = (StartPacket *)malloc(sizeof(struct tagStartPacket));
	start_recv_pkt = (StartPacket *)malloc(sizeof(struct tagStartPacket));
	pkt_with_nacks = (ControlPacket *)malloc(sizeof(struct tagControlPacket));
	last_pkt = (ControlPacket *)malloc(sizeof(struct tagControlPacket));
    //-----------------starting tcp connection
    printf("tcp : socket");
    tcpsockfd = socket(AF_INET, SOCK_STREAM, 0);
    
    if (tcpsockfd < 0) 
	    error("ERROR opening socket");
    
    server = gethostbyname(server_hostname);
    if (server == NULL) {
	    fprintf(stderr,"ERROR, no such host\n");
	    exit(0);
    }
    
    bzero((char *) &serv_addr_tcp, sizeof(serv_addr_tcp));
    serv_addr_tcp.sin_family = AF_INET;
    bcopy((char *)server->h_addr, 
	  (char *)&serv_addr_tcp.sin_addr.s_addr,
	  server->h_length);
    serv_addr_tcp.sin_port = htons(server_port_tcp);
    
    printf("tcp : connect");
    if (connect(tcpsockfd, (struct sockaddr *)&serv_addr_tcp, sizeof(serv_addr_tcp)) <0)
	    error("ERROR connecting"); 
    

    //---------------sending connection request-----------------
    
    strcpy(start_send_pkt->filename, filename);
    // Creating my address to be filled in CtrlPacket so that it can be sent 
    // to the server. The server uses this address in the sendto function UDP.

    // Creating UDP socket. Using the above address created for binding.

    printf("tcp : creating udp socket");
    udpsockfd = socket(AF_INET, SOCK_DGRAM, 0);
    if (udpsockfd < 0) 
        error("ERROR opening socket");
 

    bzero((char *) &my_addr_udp, sizeof(my_addr_udp));
    my_addr_udp.sin_family = AF_INET;
    my_addr_udp.sin_addr.s_addr = INADDR_ANY;
    my_addr_udp.sin_port = htons(server_port_udp);

    printf("tcp : binding udp");
    if (bind(udpsockfd, (struct sockaddr *) &my_addr_udp,
	     sizeof(my_addr_udp)) < 0) 
	    error("ERROR on binding");
 

    gethostname(start_send_pkt->my_hostname, 100);
    start_send_pkt->my_portno = server_port_udp;
    bzero(buffer,PACKET_SIZE);
    memcpy(buffer, start_send_pkt, sizeof(struct tagStartPacket));
    printf("tcp : sending control info on tcp");
    n = send(tcpsockfd, buffer, PACKET_SIZE, 0);
    if (n < 0)
	    error("ERROR writing to socket");

    // Receiving connection reply
    
    bzero(buffer,PACKET_SIZE);
    printf("waiting for recv");
    n = recv(tcpsockfd, buffer, PACKET_SIZE, 0);
    if (n < 0)
	    error("ERROR reading from socket");

    memcpy(start_recv_pkt, buffer, sizeof(struct tagStartPacket));
    num_of_packets = start_recv_pkt->num_packets;

    // Creating UDP thread

    pthread_create(&udp_thread, NULL, udpThread, (void *)t1);
    
    while(1) {
	    pthread_mutex_lock(&cond_mutex);
	    pthread_cond_wait(&wake_tcp, &cond_mutex);

	    printf("TCP wake up");
	    //sending the list of nACK seq nums
	    bzero(buffer,PACKET_SIZE);
	    bzero(pkt_with_nacks, sizeof(struct tagControlPacket));
	    if(myqueue.size() == 0) {
		    printf("\nbreaking!!!!!");
		    pthread_mutex_unlock(&cond_mutex);
		    break;
	    }
	    int j = 0;
	    //pkt_with_nacks->num_nacks = myqueue.size();
	    while(j < myqueue.size() && j < 700) {
		    printf("\nfilling in nack array : %d queue size : %ld", j, myqueue.size());
		    pkt_with_nacks->nack_array[j] = myqueue.at(j);
		    j++;
	    }
	    pkt_with_nacks->num_nacks = j;
	    //pkt_with_nacks->nack_vector = myqueue;
	    pkt_with_nacks->seq_num = count++;
	    bzero(buffer, PACKET_SIZE);
	    printf("\ndoing memcpy");
	    memcpy(buffer, pkt_with_nacks, sizeof(struct tagControlPacket));
	    printf("now sending");
	    n = send(tcpsockfd, buffer, PACKET_SIZE, 0);
	    printf("send done");
	    if (n < 0)
		    error("ERROR writing to socket");
	    pthread_mutex_unlock(&cond_mutex);
    }
    pthread_join(udp_thread, NULL);

    last_pkt->seq_num = count++;
    last_pkt->num_nacks = 0;
    bzero(buffer, PACKET_SIZE);
    memcpy(buffer, last_pkt, sizeof(struct tagControlPacket));
    n = send(tcpsockfd, buffer, PACKET_SIZE, 0);
    if (n < 0)
	    error("ERROR writing to socket");
    
    close(tcpsockfd);
    pthread_exit(0);
}

int main(int argc, char *argv[])
{
    long t1=1, t2=2;

    if (argc < 5) {
       fprintf(stderr,"usage %s hostname port_tcp port_udp filename\n", argv[0]);
       exit(0);
    }
    
    server_port_udp = atoi(argv[3]);
    server_port_tcp = atoi(argv[2]);
    strcpy(server_hostname, argv[1]);
    strcpy(filename, argv[4]);

    pthread_mutex_init(&cond_mutex, NULL);
    pthread_cond_init(&wake_tcp, NULL);

    pthread_create(&tcp_thread, NULL, tcpThread, (void *)t2);

    pthread_join(tcp_thread, NULL);
    return 0;
}
