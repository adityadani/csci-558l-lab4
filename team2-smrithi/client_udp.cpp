#include <cstdio>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h> 
#include <pthread.h>
#include <algorithm>
#include "packet.h"

pthread_mutex_t hash_map_mutex;
pthread_t udp_thread, tcp_thread;

std::vector<long> nack_vector;
std::map<long, DataPacket> num_packet_map;

std::vector<long>::iterator nackit;
std::map<long, DataPacket> ::iterator mapit;
std::vector<long> lost_vector;

int udpsockfd, tcpsockfd;
int server_port_tcp, server_port_udp;
char server_hostname[100], filename[100];
int num_of_packets; // Total size of the file / PAYLOAD_SIZE
int udp_end = 0;


void error(const char *msg)
{
    perror(msg);
    exit(0);
}


void check_in_nack_and_remove(long seq_num) {
	nackit = std::find(nack_vector.begin(), nack_vector.end(), seq_num);
	if(nackit != nack_vector.end()) {
		nack_vector.erase(nackit);
	}
}

void sent_nack_append_to_last(long seq_num) {
	nackit = std::find(nack_vector.begin(), nack_vector.end(), seq_num);
	if(nackit != nack_vector.end()) {
		nack_vector.erase(nackit);
		nack_vector.push_back(seq_num);
	}
}

int check_in_nack(long seq_num) {
	nackit = std::find(nack_vector.begin(), nack_vector.end(), seq_num);
	if(nackit != nack_vector.end()) {
		return 1;
	}
	else
		return 0;	
}

int check_in_lost(long seq_num) {
	nackit = std::find(lost_vector.begin(), lost_vector.end(), seq_num);
	if(nackit != lost_vector.end()) {
		return 1;
	}
	else
		return 0;	
}


int check_in_map(long seq_num) {
	mapit = num_packet_map.find(seq_num);
	if(mapit != num_packet_map.end()) {
		return 1;
	}
	else
		return 0;	
}


void *udpThread(void *id) {
	int i=0, n;

    // opening the file to write to
    struct sockaddr_in serv_addr;
    socklen_t len;
    char buffer[PACKET_SIZE];
    DataPacket *data_pkt;
    std::map<long, DataPacket>::iterator it;

	    
    // Receive Data

     int buffersize;
     socklen_t bufferlen;
     bufferlen = sizeof(buffersize);
     int res = getsockopt(udpsockfd, SOL_SOCKET, SO_RCVBUF, &buffersize, &bufferlen);
     printf("\nMy buffer size : %d", buffersize);

     long sendbuff = 444444;
     printf("\nSetting to size : %d", sendbuff);
     setsockopt(udpsockfd, SOL_SOCKET, SO_RCVBUF, &sendbuff, sizeof(sendbuff));
     res = getsockopt(udpsockfd, SOL_SOCKET, SO_RCVBUF, &buffersize, &bufferlen);
     printf("\nMy buffer size : %d", buffersize);

     data_pkt = (DataPacket *)malloc(sizeof(struct tagDataPacket));    

    int count = num_of_packets;
    long next = 1;
    int flag = 0;
    len = sizeof(struct sockaddr);
    while(1) {
		    bzero(data_pkt, sizeof(DataPacket));
		    bzero(buffer, PACKET_SIZE);
		    n = recvfrom(udpsockfd,buffer, PACKET_SIZE, 0,(struct sockaddr *) &serv_addr, &len);
		    if (n < 0) 
			    error("ERROR reading from socket");
		    
		    memcpy(data_pkt, buffer, sizeof(DataPacket));

		    printf("\nExpected : %ld \nActual : %d", next, data_pkt->seq_num);
		    if(data_pkt->seq_num == next){
				//right on time
			    pthread_mutex_lock(&hash_map_mutex);
			    num_packet_map.insert(make_pair(data_pkt->seq_num,*data_pkt));
			    pthread_mutex_unlock(&hash_map_mutex);
			    next++;
			    count--;
		    }else if(data_pkt->seq_num < next){
			    //could be a lost packet finally coming home.
			    pthread_mutex_lock(&hash_map_mutex);

			    /*it = num_packet_map.find(data_pkt->seq_num);
			    //Reason for ignoring lookup failure.if its a missing pkt,chances are its in the hash, 
			    //mapped to NULL. If not then its already been written to the file. so ignore that pkt
			    if(it != num_packet_map.end()) {	
				    //lookup success
				    if((it->second).seq_num == -1){
					    memcpy(&(it->second), data_pkt, sizeof(struct tagDataPacket)); 
					    count--;
				    }
				    
				    }*/

			    if(check_in_nack(data_pkt->seq_num) && !check_in_map(data_pkt->seq_num) && !check_in_lost(data_pkt->seq_num)) {
				    check_in_nack_and_remove(data_pkt->seq_num);
				    num_packet_map.insert(make_pair(data_pkt->seq_num,*data_pkt));
				    lost_vector.push_back(data_pkt->seq_num);
				    count--;
			    }
			    pthread_mutex_unlock(&hash_map_mutex);   


		    }else {  
			    //early bird
			    pthread_mutex_lock(&hash_map_mutex);
			    num_packet_map.insert(make_pair(data_pkt->seq_num,*data_pkt));
			    count--;

			    int nack_start = next;
			    int nack_end = data_pkt->seq_num - 1;
			    next = data_pkt->seq_num + 1;
			    for(long i=nack_start;i<=nack_end;i++){
				    
				    nack_vector.push_back(i);
				    //					data_pkt->seq_num = -1;
				    //	num_packet_map.insert(make_pair(i,*data_pkt));
				}
			    pthread_mutex_unlock(&hash_map_mutex);
			    printf("\n Next in early bird");
		    }
		    //usleep(1);
		    if(count == 0)
			    break;
		    
    }
    printf("\nNumber of pkts recvd at Client UDP thread = %d, Thread exited\n",num_of_packets);
    close(udpsockfd);
    pthread_exit(0);
}



void *tcpThread(void *id) {	
	int i=0, n, count = 1;
	long t1;
	struct sockaddr_in serv_addr_tcp, serv_addr_udp, my_addr_udp;
	struct hostent *server;
	FILE *fd;
	StartPacket *start_send_pkt, *start_recv_pkt;
	ControlPacket *pkt_with_nacks, *last_pkt;
	DataPacket *pkt_to_file;
	int seq_num_in_map, prev_seq_num = 0;
	char buffer[PACKET_SIZE], ctrl_pkt_buffer[sizeof(struct tagControlPacket)];
	char *pch, prev[50];
	int prev_vector_size = 0;
	int send_nack_num = -1;

	std::map<long, DataPacket>::iterator it;

	start_send_pkt = (StartPacket *)malloc(sizeof(struct tagStartPacket));
	start_recv_pkt = (StartPacket *)malloc(sizeof(struct tagStartPacket));
	pkt_with_nacks = (ControlPacket *)malloc(sizeof(struct tagControlPacket));
	last_pkt = (ControlPacket *)malloc(sizeof(struct tagControlPacket));
	pkt_to_file = (DataPacket *)malloc(sizeof(struct tagDataPacket));
	//-----------------starting tcp connection
	printf("\ntcp : socket");
	tcpsockfd = socket(AF_INET, SOCK_STREAM, 0);
	
	if (tcpsockfd < 0) 
		error("ERROR opening socket");
	
	server = gethostbyname(server_hostname);
	if (server == NULL) {
		fprintf(stderr,"ERROR, no such host\n");
	    exit(0);
	}
	
	bzero((char *) &serv_addr_tcp, sizeof(serv_addr_tcp));
	serv_addr_tcp.sin_family = AF_INET;
	bcopy((char *)server->h_addr, 
	      (char *)&serv_addr_tcp.sin_addr.s_addr,
	      server->h_length);
	serv_addr_tcp.sin_port = htons(server_port_tcp);
	
	printf("\ntcp : connect");
	if (connect(tcpsockfd, (struct sockaddr *)&serv_addr_tcp, sizeof(serv_addr_tcp)) <0)
		error("ERROR connecting"); 
	
	
	//---------------sending connection request-----------------
	
	strcpy(start_send_pkt->filename, filename);
	// Creating my address to be filled in CtrlPacket so that it can be sent 
	// to the server. The server uses this address in the sendto function UDP.
	
	// Creating UDP socket. Using the above address created for binding.
	
	printf("\ntcp : creating udp socket");
	udpsockfd = socket(AF_INET, SOCK_DGRAM, 0);
	if (udpsockfd < 0) 
		error("ERROR opening socket");
	
	
	bzero((char *) &my_addr_udp, sizeof(my_addr_udp));
	my_addr_udp.sin_family = AF_INET;
	my_addr_udp.sin_addr.s_addr = INADDR_ANY;
	my_addr_udp.sin_port = htons(server_port_udp);
	
	printf("\ntcp : binding udp");
	if (bind(udpsockfd, (struct sockaddr *) &my_addr_udp,
		 sizeof(my_addr_udp)) < 0) 
		error("ERROR on binding");
	
	
	gethostname(start_send_pkt->my_hostname, 100);
	start_send_pkt->my_portno = server_port_udp;
	bzero(buffer,PACKET_SIZE);
	memcpy(buffer, start_send_pkt, sizeof(struct tagStartPacket));
	printf("\ntcp : sending control info on tcp");
	n = send(tcpsockfd, buffer, PACKET_SIZE, 0);
	if (n < 0)
		error("ERROR writing to socket");
	
	// Receiving connection reply
	
	bzero(buffer,PACKET_SIZE);
	printf("waiting for recv");
	n = recv(tcpsockfd, buffer, PACKET_SIZE, 0);
	if (n < 0)
		error("ERROR reading from socket");
	
	memcpy(start_recv_pkt, buffer, sizeof(struct tagStartPacket));
	num_of_packets = start_recv_pkt->num_packets;
	printf("\n Number of packets : %d", num_of_packets);
	
	bzero(prev, 50);
	pch = strtok (filename,"/");
	while (pch != NULL) {
		strcpy(prev, pch);
		pch = strtok (NULL, "/");
	}
	fd = fopen(prev,"a");
	if(fd < 0)
		error("Error in opening file");
	
	// Creating UDP thread

	pthread_create(&udp_thread, NULL, udpThread, (void *)t1);
	
	while(1) {
		bzero(pkt_to_file, PACKET_SIZE);
		pthread_mutex_lock(&hash_map_mutex);

		printf("\nGot a lock TCP");
		mapit = num_packet_map.begin();
		memcpy(pkt_to_file, &(mapit->second), sizeof(struct tagDataPacket));
		seq_num_in_map = mapit->first;

		if((seq_num_in_map < prev_seq_num) || ((seq_num_in_map - prev_seq_num) == 1)) {		
			num_packet_map.erase(mapit);
		}
		if(!nack_vector.empty()) {
			send_nack_num = nack_vector.front();
			sent_nack_append_to_last(send_nack_num);
		}

		pthread_mutex_unlock(&hash_map_mutex);


		/*if(seq_num_in_map < prev_seq_num) {
			num_packet_map.erase(mapit);
			pthread_mutex_unlock(&hash_map_mutex);
			printf("Released lock 1");
			}*/
		if(seq_num_in_map - prev_seq_num == 1) {
			n = fwrite(pkt_to_file->data, strlen(pkt_to_file->data), 1, fd);
			printf("\nPrinting to file : %d Remaining : %d", seq_num_in_map, num_of_packets);
			prev_seq_num++;
			num_of_packets--;
			if(num_of_packets == 0) {
				udp_end = 1;
				printf("Released lock 2");
				break;
			}
		}
		else if(send_nack_num > 0) {
			bzero(ctrl_pkt_buffer,sizeof(struct tagControlPacket));
			bzero(pkt_with_nacks, sizeof(struct tagControlPacket));
			pkt_with_nacks->nack_num = send_nack_num;
			memcpy(ctrl_pkt_buffer, pkt_with_nacks, sizeof(struct tagControlPacket));
			printf("\nSent NACK : %d", pkt_with_nacks->nack_num);
			n = send(tcpsockfd, ctrl_pkt_buffer, sizeof(struct tagControlPacket), 0);
			if (n < 0) {
				error("ERROR writing to socket");
			}
			send_nack_num = -1;
		}
		usleep(200);
	}

	/*
	  
	  There is a catch here. As we do not remove the entry from the map if
	  it is a NULL packet. So every time the code comes here it will send a NACK for that packet. 
	  
	  
	  if(!num_packet_map.empty()) {
	  it = num_packet_map.begin();
	  memcpy(pkt_to_file, &(it->second), sizeof(struct tagDataPacket));
	  seq_num_in_map = it->first;
	  }
		else {
			pthread_mutex_unlock(&hash_map_mutex);
			usleep(50);
			continue;
		}
		if(pkt_to_file->seq_num == -1) {
			// a NACK entry. DO not remove
			;
		}
		else {
			// Valid packet. So remove entry from hashmap and save some space!!!!
			num_packet_map.erase(it);
		}
		pthread_mutex_unlock(&hash_map_mutex);
		
		if(pkt_to_file->seq_num == -1) {
			printf("\nMap contains NACK for seq num : %d", seq_num_in_map);
			// Map contains a NACK. So sending a NACK over TCP
			bzero(ctrl_pkt_buffer,sizeof(struct tagControlPacket));
			bzero(pkt_with_nacks, sizeof(struct tagControlPacket));
			pkt_with_nacks->nack_num = seq_num_in_map;
			memcpy(ctrl_pkt_buffer, pkt_with_nacks, sizeof(struct tagControlPacket));
			printf("\nBuffer : %s", buffer);
			n = send(tcpsockfd, ctrl_pkt_buffer, sizeof(struct tagControlPacket), 0);
			if (n < 0)
				error("ERROR writing to socket");
		}
		else {
			printf("\nMap contains packet. Writing to file. Seq num : %d Prev seq num : %d Num Packets : %d", seq_num_in_map, prev_seq_num, num_of_packets);
			// Map contains actual packet. Write to the file.
			if(seq_num_in_map - prev_seq_num == 1) {
				printf("\n1st condition");
				n = fwrite(pkt_to_file->data, strlen(pkt_to_file->data), 1, fd);
				prev_seq_num++;
			} else {
				printf("\n2nd condition");
				fseek(fd,(seq_num_in_map - 1)*PAYLOAD_SIZE,SEEK_SET);				
				n = fwrite(pkt_to_file->data, strlen(pkt_to_file->data), 1, fd);
				if(n < 0)
					error("Error writing to file");
				prev_seq_num = seq_num_in_map;
				
			}
			num_of_packets--;
			if(num_of_packets == 0)
				break;
				}*/
       	pthread_join(udp_thread, NULL);
	
	last_pkt->nack_num = -1;
	bzero(buffer, PACKET_SIZE);
	memcpy(buffer, last_pkt, sizeof(struct tagControlPacket));
	n = send(tcpsockfd, buffer, PACKET_SIZE, 0);
	if (n < 0)
		error("ERROR writing to socket");
	
	fclose(fd);
	close(tcpsockfd);
	pthread_exit(0);
}

int main(int argc, char *argv[])
{
    long t1=1, t2=2;

    if (argc < 5) {
       fprintf(stderr,"usage %s hostname port_tcp port_udp filename\n", argv[0]);
       exit(0);
    }
    
    server_port_udp = atoi(argv[3]);
    server_port_tcp = atoi(argv[2]);
    strcpy(server_hostname, argv[1]);
    strcpy(filename, argv[4]);

    pthread_mutex_init(&hash_map_mutex, NULL);

    pthread_create(&tcp_thread, NULL, tcpThread, (void *)t2);

    pthread_join(tcp_thread, NULL);
    return 0;
}
