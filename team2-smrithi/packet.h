#include <cstdio>
#include <vector>
#include <map>

using std::map;
using std::make_pair;
using std::vector;

#define PACKET_SIZE 1480
#define PAYLOAD_SIZE (PACKET_SIZE - sizeof(long) - 2)

typedef struct  tagDataPacket{
	long seq_num;
	bool eof;
	char data[PAYLOAD_SIZE];
}DataPacket;

typedef struct tagStartPacket{
	char filename[50];
	char my_hostname[100];
	int my_portno;
	long num_packets;
}StartPacket;

typedef struct tagControlPacket{
	long nack_num;
}ControlPacket;
